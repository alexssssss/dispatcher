<?php

namespace Alexssssss\Dispatcher;

class Rest implements DispatchMethodInterface
{

    protected $notCallableActions = ['init', 'beforeAction', 'afterAction'];

    /**
     *
     * @var \Auryn\Injector
     */
    protected $injector;

    /**
     *
     * @param \Auryn\Injector $injector
     */
    public function __construct(\Auryn\Injector $injector)
    {
        $this->injector = $injector;
    }

    /**
     *
     * @param string $restController
     * @param string $action
     * @param array $args
     * @param string|null $controllerIsSubclassOf
     * @param string|null $controllerNamespace
     * @return mixed
     * @throws Exception\General
     */
    public function run($restController, $action = '', $args = [], $controllerIsSubclassOf = null, $controllerNamespace = null)
    {

        if (is_object($restController)) {
            $fullClassName = get_class($restController);
        } else {
            $restController = str_replace('-', '', $restController);
            $fullClassName = str_replace('/', '\\', trim($controllerNamespace . '\\' . $restController, '\\'));

            // classes allways start with a uppercase char.
            $fullClassNameArray = [];
            foreach (explode('\\', $fullClassName) as $class) {
                $fullClassNameArray[] = ucfirst($class);
            }
            $fullClassName = implode('\\', $fullClassNameArray);

            if (!class_exists($fullClassName) && class_exists($fullClassName . '\\Index')) {
                $fullClassName .= '\\Index'; // load the index class of a namespace if non specified
            }
        }

        $action = str_replace('-', '', $action);

        $method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
        if ($method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $method = 'DELETE';
            } elseif ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $method = 'PUT';
            } else {
                throw new Exception\BadRequest('REST dispatcher received POST requests with unexpected value for \'HTTP_X_HTTP_METHOD\' header', 500);
            }
        }

        if ($method == 'POST' && $_SERVER['REQUEST_CONTENT_TYPE'] && stripos($_SERVER['REQUEST_CONTENT_TYPE'], 'application/json') !== false) {
            $postdata = file_get_contents("php://input");
            $_POST = json_decode($postdata, true);
        } elseif ($method == 'PUT') {
            $putdata = file_get_contents("php://input");

            if ($_SERVER['REQUEST_CONTENT_TYPE'] && stripos($_SERVER['REQUEST_CONTENT_TYPE'], 'application/json') !== false) {
                $GLOBALS['_PUT'] = json_decode($putdata, true);
            } else {
                $GLOBALS['_PUT'] = $putdata;
            }
        } elseif ($method == 'DELETE') {
            $deletedata = file_get_contents("php://input");

            if ($_SERVER['REQUEST_CONTENT_TYPE'] && stripos($_SERVER['REQUEST_CONTENT_TYPE'], 'application/json') !== false) {
                $GLOBALS['_DELETE'] = json_decode($deletedata, true);
            } else {
                $GLOBALS['_DELETE'] = $deletedata;
            }
        }

        if (!class_exists($fullClassName)) {
            throw new Exception\NotFound('REST dispatcher can\'t find class \'' . $fullClassName . '\' OR \'' . $fullClassName . '\\Index\'', 404);
        } elseif ($controllerIsSubclassOf !== null && !is_subclass_of($fullClassName, $controllerIsSubclassOf)) {
            throw new Exception\Forbidden('REST dispatcher won\'t dispatch class \'' . $fullClassName . '\' because it\'s nog a subclass/implementation of \'' . $this->controllerIsSubclassOf . '\'');
        } elseif (strtolower($method) === 'options') {
            if (is_callable(array($fullClassName, $action))) {
                header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE');
            } else {
                $methods = [];

                if (is_callable(array($fullClassName, 'post' . ucfirst($action)))) {
                    $methods[] = 'POST';
                }
                if (is_callable(array($fullClassName, 'get' . ucfirst($action)))) {
                    $methods[] = 'GET';
                }
                if (is_callable(array($fullClassName, 'put' . ucfirst($action)))) {
                    $methods[] = 'PUT';
                }
                if (is_callable(array($fullClassName, 'delete' . ucfirst($action)))) {
                    $methods[] = 'DELETE';
                }

                header('Access-Control-Allow-Methods: ' . implode(', ', $methods));
            }

            return null; // return empty if no specifications are set for the options method.
        } elseif (!($restController = $this->injector->make($fullClassName, $args))) {
            throw new Exception\General('REST dispatcher can\'t build controller class');
        } elseif (in_array($action, $this->notCallableActions) || (isset($restController->notCallableActions) && in_array($action, (array)$restController->notCallableActions))) {
            throw new Exception\Forbidden('REST dispatcher won\'t dispatch the request, action marked as not callable');
        } elseif (!is_callable(array($restController, strtolower($method) . ucfirst($action))) && !is_callable(array($restController, $action))) {
            throw new Exception\NotFound('Dispatcher can\'t call \'' . $action . '\' or \'' . strtolower($method) . ucfirst($action) . '\'  of class \'' . $fullClassName . '\'');
        } else {
            if (is_callable(array($restController, 'init'))) {
                $this->injector->execute(array($restController, 'init'), $args);
            }
            if (is_callable(array($restController, 'beforeAction'))) {
                $this->injector->execute(array($restController, 'beforeAction'), $args);
            }

            if (is_callable(array($restController, strtolower($method) . ucfirst($action)))) {
                $return = $this->injector->execute(array($restController, strtolower($method) . ucfirst($action)), $args);
            } else {
                $return = $this->injector->execute(array($restController, $action), $args);
            }

            if (is_callable(array($restController, 'afterAction'))) {
                $this->injector->execute(array($restController, 'afterAction'), $args);
            }

            return $return;
        }
    }

    /**
     *
     * @param string $route
     * @return array [restController, section, args]
     */
    public static function routeToParts($route)
    {
        $args = [];
        $parts = explode('/', rtrim($route, '/'));

        $action = '';

        foreach ($parts as $key => $part) {
            if (ctype_digit($part)) {
                $parentkey = $key - 1;
                if (!isset($parts[$parentkey])) {
                    $parentkey--;
                }
                $args[':' . $parts[$parentkey] . 'Id'] = $part;
                unset($parts[$key]);
            }
        }

        if (count($parts) > 1) {
            $action = array_pop($parts);
        }

        $restController = implode('/', $parts);

        return [$restController, $action, $args];
    }
}
