<?php

namespace Alexssssss\Dispatcher;

interface DispatchMethodInterface
{

    public function run($param);
}
