<?php

namespace Alexssssss\Dispatcher;

class Controller implements DispatchMethodInterface
{

    protected $notCallableActions = ['init', 'beforeAction', 'afterAction'];

    /**
     *
     * @var \Auryn\Injector
     */
    protected $injector;

    /**
     *
     * @param \Auryn\Injector $injector
     */
    public function __construct(\Auryn\Injector $injector)
    {
        $this->injector = $injector;
    }

    /**
     *
     * @param string $controller
     * @param string $action
     * @param array $args
     * @param string|null $controllerIsSubclassOf
     * @param string|null $controllerNamespace
     * @return mixed
     * @throws Exception\General
     */
    public function run($controller, $action = "index", $args = [], $controllerIsSubclassOf = null, $controllerNamespace = null)
    {

        if (empty($action)) {
            $action = 'index';
        }

        if (is_object($controller)) {
            $fullClassName = get_class($controller);
        } else {
            $controller = str_replace('-', '', $controller);
            $fullClassName = str_replace('/', '\\', trim($controllerNamespace . '\\' . $controller, '\\'));

            // classes allways start with a uppercase char.
            $fullClassNameArray = [];
            foreach (explode('\\', $fullClassName) as $class) {
                $fullClassNameArray[] = ucfirst($class);
            }
            $fullClassName = implode('\\', $fullClassNameArray);

            if (!class_exists($fullClassName) && class_exists($fullClassName . '\\Index')) {
                $fullClassName.='\\Index'; // load the index class of a namespace if non specified
            }
        }

        $action = str_replace('-', '', $action);

        if (!is_object($controller) && !class_exists($fullClassName)) {
            throw new Exception\NotFound('CONTROLLER dispatcher can\'t find class \'' . $fullClassName . '\' OR \'' . $fullClassName . '\\Index\'', 404);
        } elseif (strncmp($action, '_', 1) === 0) {
            throw new Exception\BadRequest('CONTROLLER dispatcher won\'t dispatch a function starting with a \'_\'');
        } elseif ($controllerIsSubclassOf !== null && !is_subclass_of($fullClassName, $controllerIsSubclassOf)) {
            throw new Exception\Forbidden('CONTROLLER dispatcher won\'t dispatch class \'' . $fullClassName . '\' because it\'s nog a subclass/implementation of \'' . $this->controllerIsSubclassOf . '\'');
        } elseif (!is_object($controller) && !($controller = $this->injector->make($fullClassName, $args))) {
            throw new Exception\General('CONTROLLER dispatcher can\'t build controller class');
        } elseif (in_array($action, $this->notCallableActions) || (isset($controller->notCallableActions) && in_array($action, (array) $controller->notCallableActions))) {
            throw new Exception\Forbidden('CONTROLLER dispatcher won\'t dispatch the request, action marked as not callable');
        } elseif (!is_callable(array($controller, $action))) {
            throw new Exception\NotFound('Dispatcher can\'t call \'' . $action . '\' of class \'' . $fullClassName . '\'');
        }

        if (is_callable(array($controller, 'init'))) {
            $args[':initReturn'] = $this->injector->execute(array($controller, 'init'), $args);
        }
        if (is_callable(array($controller, 'beforeAction'))) {
            $args[':beforeActionReturn'] = $this->injector->execute(array($controller, 'beforeAction'), $args);
        }

        $output = $this->injector->execute(array($controller, $action), $args);
        $args[':output'] = $output;

        if (is_callable(array($controller, 'afterAction'))) {
            return $this->injector->execute(array($controller, 'afterAction'), $args);
        }

        return $output;
    }
}
