<?php

namespace Alexssssss\Dispatcher;

class File implements DispatchMethodInterface
{

    protected $restrictedFileNames = ['con', 'nul', 'prn', 'lpt1', 'lpt2', 'aux', 'com1', 'com2', 'com3', 'com4'];

    public function run($file, $contentAbsolutePath = ".", $restrictedFileNames = array())
    {
        $restrictedFileNames = array_merge($this->restrictedFileNames, $restrictedFileNames);

        if (in_array(strtolower($file), $restrictedFileNames)) {
            throw new Exception\Forbidden('FILE dispatcher can\'t load restricted file \'' . $file . '\'', 403);
        } elseif (!is_file($contentAbsolutePath . "/" . $file . ".php")) {
            throw new Exception\NotFound('FILE dispatcher can\'t find file \'' . $file . '\'', 404);
        } elseif (!preg_match("%^" . preg_quote(realpath($contentAbsolutePath . "/")) . "%", realpath($contentAbsolutePath . "/" . $file . ".php"))) {
            throw new Exception\Forbidden('FILE dispatcher can\'t load file \'' . $file . '\' it\'s outside the content absolute path', 403);
        }

        foreach ($GLOBALS as $key => $value) {
            if (!isset(${$key})) {
                global ${$key};
            }
        }

        ob_start();

        include $contentAbsolutePath . "/" . $file . ".php";
        $output = ob_get_clean();

        $GLOBALS += get_defined_vars();

        return $output;
    }
}
