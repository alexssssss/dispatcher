<?php

namespace MyNamespace;

class MyController
{

    public function __construct()
    {
        
    }

    public function index()
    {
        
    }

    public function action()
    {
        
    }
    
    public function init()
    {
        
    }

    public function beforeAction()
    {
        
    }

    public function afterAction()
    {
        
    }
}
